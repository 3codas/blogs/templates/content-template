const axios = require("axios");
const util = require("util");
const exec = util.promisify(require("child_process").exec);

const REQUIRE_ARGS = 4;

const usage = () => {
  console.info("Usage:");
  console.info(" $> node script.js <project_id> <merge_request_id>");
};

const exit_with_message = (message) => {
  console.error(message);
  process.exit(1);
};

const exit_info_with_message = (message) => {
  console.info(message);
  process.exit(0);
};

const create_object_articles = (
  id,
  title,
  author,
  path,
  description,
  image_src
) => {
  return {
    id,
    title,
    author,
    publish_at: Date.now().toString(),
    path,
    description,
    image_src,
  };
};

if (process.argv.length < REQUIRE_ARGS) {
  console.error(
    `Error: You should provide at least ${REQUIRE_ARGS - 2} arguments`
  );
  usage();
  process.exit(1);
}

const parse_description = (description_raw) => {
  let info = {};
  description_raw.split("\n").forEach((line) => {
    const line_s = line.split(":");
    if (line_s.length < 2) {
      exit_with_message(`Error: malformat description for line: ${line}`);
    }
    if (line_s.length > 2) {
      line_s[1] = line_s.slice(1).join(":");
    }

    info[line_s[0].toLowerCase()] = line_s[1].trim();
  });

  return info;
};

const API_GITLAB_TOKEN = process.env.API_GITLAB_TOKEN;
const INFO_CATEGORY_FILE = process.env.INFO_CATEGORY_FILE || "info.json";
const DEFAULT_BRANCH_REF = process.env.DEFAULT_BRANCH_REF || "main";
const AUTO_PUBLISH_BOT_EMAIL =
  process.env.AUTO_PUBLISH_BOT_EMAIL || "bot@example.com";
const AUTO_PUBLISH_BOT_NAME =
  process.env.AUTO_PUBLISH_BOT_NAME || "BOT Auto-Publish";

const project_id = process.argv[2];
const merge_request_id = process.argv[3];

const main = async () => {
  const req = await axios.get(
    `https://gitlab.com/api/v4/projects/${project_id}/merge_requests/${merge_request_id}`
  );

  if (req.status != 200) {
    exit_with_message(`Error: Api give ${req.status} status code.`);
  }
  const rep = req.data;

  if (!rep.source_branch.startsWith("content/")) {
    exit_info_with_message(
      "Info: Source branch doesn't starts with 'content/'. Leaving the script now!"
    );
  }

  if (
    rep.draft ||
    rep.title.startsWith("Draft:") ||
    rep.title.startsWith("draft:")
  ) {
    exit_info_with_message(
      "Info: Source branch title contains Draft:|draft: or merge request is set to draft. Leaving the script now!"
    );
  }

  const commit_sha = rep.sha;

  const { stdout, stderr } = await exec(
    `git diff-tree --no-commit-id --name-only -r ${commit_sha}`
  );

  if (stderr) {
    exit_with_message(`Error: ${stderr}`);
  }

  const files = stdout.split("\n").filter((str) => str);

  if (files.length != 1) {
    exit_with_message(`Error: More than one file in that MR.`);
  }

  const file_path = files[0];
  const file_category_split = file_path.split("/");

  if (file_category_split.length != 2) {
    exit_with_message(`Error: file path doesn't belong to one category`);
  }

  const file_category = file_category_split[0];
  const file_name = file_category_split[1];

  const req2 = await axios.get(
    `https://gitlab.com/api/v4/projects/${project_id}/repository/files/${encodeURIComponent(
      file_path
    )}?ref=${DEFAULT_BRANCH_REF}`
  );

  if (req2.status != 200) {
    exit_with_message(
      `Error: Api give ${req.status} status code when fetching file info.`
    );
  }

  if (req2.data.file_name != file_name) {
    exit_with_message(
      `Error: File named from file_path doesn't match real file_name`
    );
  }

  const category_file_info_path = `${file_category}/${INFO_CATEGORY_FILE}`;

  const req3 = await axios.get(
    `https://gitlab.com/api/v4/projects/${project_id}/repository/files/${encodeURIComponent(
      category_file_info_path
    )}/raw?ref=${DEFAULT_BRANCH_REF}`
  );

  if (req3.status != 200) {
    exit_with_message(
      `Error: Api give ${req.status} status code when fetching category file info.`
    );
  }

  const category_info = req3.data;

  const { title, author, description, image_src } = parse_description(
    rep.description
  );

  const obj_arcticle = create_object_articles(
    req2.data.blob_id,
    title,
    author,
    file_path,
    description,
    image_src
  );

  if (
    category_info.articles.find(
      (search) =>
        search.id == obj_arcticle.id || search.path == obj_arcticle.path
    )
  ) {
    exit_with_message(
      `Error: Id or path for articles ${title} already exist (${obj_arcticle.id} / ${obj_arcticle.path})`
    );
  }

  category_info.articles.push(obj_arcticle);

  const res = await axios.put(
    `https://gitlab.com/api/v4/projects/${project_id}/repository/files/${encodeURIComponent(
      category_file_info_path
    )}`,
    {
      branch: DEFAULT_BRANCH_REF,
      author_email: AUTO_PUBLISH_BOT_EMAIL,
      author_name: AUTO_PUBLISH_BOT_NAME,
      content: JSON.stringify(category_info, null, 2),
      commit_message: `Publish articles ${title}`,
    },
    {
      headers: {
        "PRIVATE-TOKEN": API_GITLAB_TOKEN,
      },
    }
  );

  if (res.status != 200) {
    exit_with_message(
      `Error: Publish articles failed. API Give error ${res.status}.`
    );
  }
  console.info(`Publish articles ${title}`);
};

main();
