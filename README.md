# content-template

This repository is a template to make it easy to create a blog and use Gitlab as a backend.
This repository also contain a way to auto-publish articles on merge to the `main` branch using Pipelines and the `script.js` file.

## Article redaction

In order to make this repository work, you need to use the conventions defines in the [./docs/publication.md](/docs/publication.md) file.

## Dependencies

To use the node-script, you need to have git AND node install.

## Node script

The script as been write using the node version `16.13.1` and should be use with a container who have at least the same version or upper.

#### Usage

To run the script, you first need to set some environments variable:

1. **API_GITLAB_TOKEN**: A token with write access to this repositories. You can find more informations in the [documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), in particular how to generate a token.
2. **INFO_CATEGORY_FILE**: The file name of the file that contains all articles in a category (Default to `info.json`).
3. **DEFAULT_BRANCH_REF**: The default branch ref (the main branch of your repository). (Default to `main`).
4. **AUTO_PUBLISH_BOT_EMAIL**: The bot email to use to commit change in file. (Default to `bot@example.com`).
5. **AUTO_PUBLISH_BOT_NAME**: The bot name in the commit. (Default to `BOT Auto-Publish`).

You can now run the script:

```bash
$ node script.js <project_id> <merge_request_id>
```

This script will try to update the **INFO_CATEGORY_FILE** with information provide in the merge-request.
To make this script work, you need to check the [./docs/publication.md](/docs/publication.md) file.

#### To make script works

In order to use this scripts, some pattern should be respect.

1. Any merge-request which aim to create an article should be create using a branch who start with "content/".
2. Any merge-request who are in draft or title starts with "Draft:" or "draft:" will not been publish
3. A merge-request with more than one file change in it will not-be publish
4. An article with an already existing blob id or path will not be publish
5. Any connection / network / API error will leave the script
6. If the merge_request description doesn't match the template description, script will leave.
7. If the file_name found by splitting path and the real file_name doesn't match, the script will end.
