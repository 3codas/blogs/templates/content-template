# Publication

Ce fichier vise à exposer les pratiques et étapes à réaliser afin de rédiger et de publier un article.

### Prérequis

Avant de rédiger un article, il est **indispensable** d'avoir créé une catégorie.
Je conseille ainsi de créer une catégorie et en amont de copier dans le presse papier le contenu du fichier [categorie.template.json](categorie.template.json).
La création de catégorie est nécessaire seulement si l'article ne peux être mis dans une catégorie existante.

### Création d'une catégorie

##### A partir du site de gestion de contenu

Bientot...

##### Création d'une catégorie à partir du site GitLab

Pour commencer, il faut appuyer sur le bouton `+` sur la [page principale](https://gitlab.com/3codas/blogs/templates/content-template) et appuyer sur le bouton `New directory`.
![cc1](./images/cc1.png)
Une popup s'ouvre vous demandant d'indiquer le nom du dossier à créer et un message de commit. Le nom du dossier doit etre le nom de la catégorie en remplacant les espaces par des tirets.
![cc2](./images/cc2.png).
Dans l'example, j'ai choisi d'appeler le dossier `Pain` et mis à jour le message du commit en fonction.
Une fois le dossier créé, il faut créer le fichier `info.json`. Dans le dossier que vous venez de créer, il suffit d'appuyer sur le bouton `+` et le bouton `New file`.
![cc3](./images/cc3.png)
Une nouvelle fenetre s'ouvre, dans le champ en haut il suffit de mettre `info.json`. Pour le contenu du fichier, il faut tout d'abord copier le contenu du fichier `categorie.template.json` puis le coller dans le carré noir.
![cc4](./images/cc4.png)
Il suffit ensuite de remplacer les informations dans le fichier (voir deux section en deux dessous sur la description du fichier)
![cc5](./images/cc5.png)
Ensuite, il faut modifier le messsage du commit dans le champ, comme j'ai pu l'effectuer dans l'exemple. Il suffit ensuite d'appuyer sur le bouton `commit changes`.
![cc6](./images/cc6.png)
Un fichier `info.json` devrait alors être créer dans le dossier. La catégorie est donc prête à être utilisé.
![cc7](./images/cc7.png)

##### Création d'une catégorie à partir d'un terminal

Il suffit de créer un dossier avec un fichier `info.json` dedans. Un template de ce fichier existe sous le nom [categorie.template.json](/categorie.template.json).

```sh
$ mkdir <nom_de_la_categorie>
$ cp categorie.template.json <nom_de_la_categorie>/info.json
```

Il suffit ensuite de modifier le fichier `<nom_de_la_categorie>/info.json`.

Après modification, pusher la catégorie en ligne :

```sh
$ git add <nom_de_la_categorie>/info.json
$ git commit -m "feat: Ajout de la categorie <nom_de_la_categorie>"
$ git push
```

(Ou créer une branche puis une merge request)

##### Description du fichier info.json

Le template du fichier ressemble à celui ci-dessous.

```json
{
  "id": "un id aleatoire différent de ce qu'il existe (cela peut etre le nom de la categorie)",
  "path": "le nom du dossier",
  "title": "Le titre de la categorie qui sera affiché",
  "image_src": "lien illustration",
  "articles": []
}
```

Pour chaque champs :

- **id** : Il s'agit d'un id aléatoire, je conseille de mettre le nom de la categorie en minuscule.
- **path** : Le nom littéral du dossier créé.
- **title** : Le titre de la catégorie qui sera affiché sur le site.
- **image_src** : Une image d'illustration (idéalement mis en ligne sur le cloud interne).
- **articles** : Laisser le tableau vide

### Rédaction d'un article

Avant de rédiger un article, assurez vous de l'existance de sa catégorie.

La rédaction de l'article doit se faire sous le format Markdown.

##### Rédaction à partir du site de gestion de contenu

Bientot...

##### Rédaction d'un article sur GitLab

Pour commencer, il faut créer une nouvelle branche. Sur la [page principale](https://gitlab.com/3codas/blogs/templates/content-template) de GitLab, il faut appuer sur le boutont `+` et appuyer sur le bouton `New branch`.
![ra1.png](./images/ra1.png)
Sur la nouvelle page ouverte, vous devez mettre le nom de la branche **ATTENTION La branche doit absolument commencer par `content/`**.
Vous pouvez nommer la branche de cette manière: `content/<nom_de_larticle>`, en remplacant tout les espaces par un `_` et en ne mettant pas les caractères spéciaux (par exemple: ',./ ou autre).
Dans le champ `Create from` il faut laisser `main`. Vous pouvez ensuite appuyer sur le bouton `Create branch`.
![ra2](./images/ra2.png)
Vous êtes maintenant sur votre nouvelle branche de travail comme le montre la petite drop-down contenant le nom de votre branche. Si ce n'est pas le nom de votre branche qui apparait à cette endroit, selectionnez la pour basculer sur cette dernière.
![ra3](./images/ra3.png)
Maintenant que votre branche est créé, vous pouvez créer un nouveaux fichier dans la catégorie de votre choix. Pour l'example, nous utiliserons la catégorie `Pain`. Cliquer sur le dossier de votre catégorie pour arriver à une vue détaillé des fichiers que contient ce dossier. Le dossier devrait au moins contenir un fichier `info.json`.
Pour créer un nouveau fichier, il suffit de cliquer sur le bouton `+` puis sur le bouton `New file`.
![ra4](./images/ra4.png)
Une nouvelle page apparait. Dans le champ de formulaire (marqué par _File name_), vous devez y apposer le nom du fichier que vous voulez créer, je conseille de mettre le nom de l'article, toujours sans caractéres spéciaux et en remplacant les espaces par `_`.
**Attention: tout les nom des fichiers d'articles doivent finir par `.md`**.
Dans l'encart noir, vous pouvez y rédiger votre article sous le format markdown.
Pour le champ `commit message`, je conseille de changer le message vers quelque chose de plus parlant, tel que _création de l'article ..._.
Dans le champ `Target Branch` vous devriez retrouvez le nom de votre branch précedement crée.
Une fois l'article rédiger ou simplement pour le sauvegarder, vous pouvez appuyer sur le bouton `Commit changes`.
![ra5](./images/ra5.png)
En appuyant sur le bouton `Commit changes`, vous devriez arriver sur la page de votre fichier.
Vous pouvez le modifier en appuyant sur le bouton `edit`
Pour proposer, puis publiez votre article, il faut créer une _merge request_.
Pour créer une merge request, vous pouvez utiliser le bouton bleu `Create merge request` ou cliquer sur merge request dans la sidebar.
![ra6.png](./images/ra6.png)
Si vous cliquer sur la sidebar, vous arrivez à l'écran suivant.
![ra7.pnh](./images/ra7.png)
Vous devez ainsi cliquer sur le bouton bleu `New merge request` vous amenant à l'écran suivant :
![ra8.pnh](./images/ra8.png)
Vous devez ainsi sélectionnez à gauche votre branche que vous avez créer. Une fois sélectionné, vous pouvez cliquer sur le bouton `Compare branches and continue`.
Vous arrivez sur un nouvel écran.
![ra9.png](./images/ra9.png)
Pour le titre (champ `title`) Vous pouvez y mettre celui de votre choix, je conseille `Content: création de l'article <titre>`
Pour ce qui est de la description, vous devez choisir un template. Dans le dropdown button marquer par Choose a template, vous devez selectionner le template `articles`.
Une fois sélectionnez, votre écran devrait ressembler au suivant :
![ra10.png](./images/ra10.png).
Pour chaque des champs dans la description, vous devez les remplacer par les informations adéquates:
Vous devez ainsi remplacer:

- \<title>: par le titre que vous souhaitez pour votre article
- \<author>: par votre nom et prénom sous le format NOM Prénom
- \<description>: Une bréve description de l'article qui pourrait être afficher sur une carte de présentation
- \<image_src>: Une url menant vers une image, idéalement mise en ligne sur le cloud de 3codas.

Une fois la description modifié, vous pouvez cliquer sur le bouton `assign to me` puis descendre en bas de la page pour cliquer sur le bouton `Create merge request`.
Vous devriez arriver sur une nouvelle page
![ra11.png](./images/ra11.png)
Chaque modification de l'article sur votre branche entrainera la modification aussi sur votre merge request. Une fois votre article finis, vous pouvez cliquer sur le bouton `Merge`.
Une pipeline automatique publiera votre article sur le blog liés.

##### Rédaction d'un article sur un terminal

Après avoir cloné le repo, il suffit de créer une branche commencant par `content/`

```sh
$ git checkout -b content/<...>
```

Dans le dossier de la catégorie auquel appartient l'article, vous pouvez créer et rédiger l'article.

```
$ cd <categorie>
$ touch <nom_article>.md
$ vim <nom_article>.md
```

Une fois la rédaction faites, il suffit de push sur le repo votre branche et de créer une merge request dans gitlab.

```
$ git add <categorie>/<nom_article>.md
$ git commit -m "message"
$ git push -u origin content/<...>
```

Dans Gitlab, il faut utiliser le template de merge_request s'appellant `article` et le remplir avec les informations demandés.

Une fois le processus de review, vous pouvez merger l'article dans la branche `main` en utilisant le bouton.
Une pipeline devrait s'activer et démarrer l'auto-publication.

## Publication

Toute article respectant les conditions ci-dessous sera automatiquement publié lorsque la branche sera mergé dans la branche main.

Condition:

- La branche de l'article commence par `content/`
- Le titre de la merge request ne commence pas par `Draft:` ou `draft:` ou est dans un état _Draft_
- L'article n'à jamais été publié (path ou id non existant)
- La merge request ne contient qu'un seul et unique changement : le fichier du nouvel article

D'autre condition peuvent provoquer l'arret de la publication :

- Un problème de connection pendant la publication
- Des noms de fichiers ne correspondant pas.
